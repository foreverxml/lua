-- geometry script
local Static,Class = require("static"), require("class")
local Types = Static:Enum("Triangle","TriPri","RiTri","Square","Cube","Rect","Prism","Circle","Sphere","Cylinder")
local Geometry = Class:New()
local repeats = Static:New(function(tab,f)
	for _,v in pairs(tab) do
		f(v)
	end
end)
repeats()({Types.Triangle,Types.TriPri,Types.RiTri,Types.Square,Types.Cube,Types.Circle,Types.Sphere,Types.Rect,Types.Prism,Types.Cylinder},function(v) Geometry[v] = Class:New() end)
function Geometry:new(typen,...)
	local bool = Static:new(false)
	for _,v in pairs(Types) do
		if typen == v then bool = true end
	end
	if not bool then
		error("Not a geo type!",2)
	end
	return Self[typen]:New(...)
end
function Geometry.Triangle:Area(b,h) return b * h * 0.5 end -- GEOFUNC: Triangle:Area(base, height) to calculate triangle area
function Geometry.Triangle:SideLength(b,h) return math.sqrt((b / 2) ^ 2 + h ^ 2) end -- GEOFUNC: Triangle:SideLength(base, height) Calculates side lengths of a triangle where base and height are known.

-- GEOFUNC: Triangle:New(base, height) creates a new triangle with base base and height height.
function Geometry.Triangle:New(b,h)
	local tri = {base = b, height = h}
	tri.area = self:Area(b,h)
	tri.sides = self:SideLength(b,h)

	-- GEOFUNC: TriangleObject:Update(base, height) will update all params for TriangleObject and return TriangleObject.
	function tri:Update(b,h)
		self.base = b
		self.height = h
		self.area = b * h * 0.5
		self.sides = math.sqrt((b / 2) ^ 2 + h ^ 2)
		return self
	end

	return setmetatable(tri,{
		__tostring = function(tri) return "Triangle with base "..tri.base.." and height "..tri.height end
	})
end
Geometry.Triangle:SetCall(function() print("Use Triangle:New, Triangle:Area, or Triangle:SideLength with number arguments base, height.") end)

function Geometry.Square:Area(b) return b * b end -- GEOFUNC: Square:Area(base) returns area of a square with side length of base.

-- GEOFUNC: Square:New(base) creates a square with a side length of base.
function Geometry.Square:New(b)
	local sq = {base = b}
	sq.area = self:Area(b)

	-- GEOFUNC: SquareObject:Update(base) does what TriangleUpdate does, just for SquareObject.
	function sq:Update(b)
		sq.base = b
		sq.area = b * b
		return self
	end

	return setmetatable(sq,{
		__tostring = function(sq) return "Square with side length of "..sq.base end
	})
end
Geometry.Square:SetCall(function() print("Use Square:New or Square:Area with number argument base.") end)
Geometry:SetCall(function() print("Look at geo.lua at codeberg.org/foreverxml/lua or in your deps for exact help with functions (GEOFUNC).") end)
return Geometry
