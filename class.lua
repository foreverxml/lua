#!/usr/bun/lua

-- from roblox/luau wiki
local function deepCopy(original)
	local copy = {}
	for k, v in pairs(original) do
		if type(v) == "table" then
			v = deepCopy(v)
		end
		copy[k] = v
	end
	return copy
end
--end

-- create Class
local Class = setmetatable({},{
	__call = function()
		print("CLASS: Use Class:New to create a new class.\n\nOn a class (MAIN) you can do this:\n- edit what MAIN() does: \"MAIN:SetCall(function)\"\n- deep copy the class \"MAIN:Copy()\"\n- make a new function by using lua \"--[[ local ]] function MAIN:Func() --[[ body ]] end\".") end
})

-- new class func
function Class:New()
	local cla = {params = {class = "Class"}}
	function cla:SetCall(f)
		local mt = getmetatable(self)
		mt.__index = f
		return setmetatable(self,mt)
	end
	function cla:Copy() return setmetatable(deepCopy(self),deepCopy(getmetatable(self))) end
	return setmetatable(cla,{__call = function()
		print("This CLASS has no call function yet. Use method Class:SetCall(function) where Class is the name of the CLASS.")
	end})
end

-- and return Class Class
return Class
