#!/usr/bin/lua

-- My first Lua code! Hooray!

-- Defining variables

-- Defining custom character/string cor clist in rtype table
Static = require("static") -- from codeberg.org/foreverxml/lua
local Types = Static:Enum("number", "list", "custom", "exit")
local clistchar = Static:New("")

-- function to test emptiness
function is_empty (var)
	if (var == true) then
		return true
		-- "", 0, and nil equal true in Lua
	end
	return false
end

-- make string into table
function listeval(list, char)
	-- tysm StackOverflow
	if char == nil then char = "%s" end
        local tab={}
        for str in string.gmatch(list, "([^"..char.."]+)") do table.insert(tab, str) end
	return Static:New(tab)
end

-- random generator
function rand(numr)
	-- set seed
	math.randomseed(os.time())
	-- make random...
	local n = math.random()
	-- multiply by input...
	n = n * numr
	-- subrtact 1...
	n = n - 1
	-- ...for the return ceil
	return math.ceil(n)
end

-- list random
function randlist(tab)
	local ran = rand(#tab)
	io.write("\nYour result was:\n\n"..tab[ran].."\n\nExiting now...\n\n\n")
end

-- random number
function numgen (ntab)
	-- subtract nums for num difference (see why -1 later)
	local num = ntab[2] - ntab[1]
	-- generate random
	local ran = rand(num) + ntab[1]
	-- then print it!
	io.write("\nYour random number is "..ran.."! Exiting now...\n\n\n")
end

-- main
function main ()
	str = nil
	-- greetings!

	local types
	io.write("Hi! You are in Lua Randomizer. Choose an option by writing \"number\", \"list\", \"custom\", or \"exit\" and pressing \"Enter\": ")
	str = Static:New(io.read())
	-- get user input? if wrong type Types.number assumed
	if (str.value == Types.number) or (str.value == Types.list) or (str.value == Types.custom) then
		types = Static:New(str.value)
	else types = Static:New(Types.number) end

	-- then the cascade
	if types == Types.number or str.value == Types.number then
		-- make Numbers array
		local nums = {}

		-- grab min number
		io.write("\nWrite your minumum/low number here (required): ")
		strn = Static:New(io.read("*number"))
		if strn.value == true then strn.value = 1 end
		nums[1] = strn()

		-- grab max number
		io.write("\nWrite your maximum/high number here (required): ")
		strn = strn:Change(io.read("*number"))
		if strn() == true then strn = strn:Change(10) end
		nums[2] = strn()

		-- gen random number
		numgen(nums)
	elseif types == Types.list or str.value == Types.list then
		local list

		-- grab the list
		io.write("\nWrite your list, seperating items with a / and no space: ")
		str = str:Change(io.read())
		randlist(listeval(str(),"/"))
	elseif types == Types.custom or str.value == Types.custom then
		local list

		-- char
		io.write("\nWrite your seperation phrase here: ")
		str = str:Change(io.read())
		local ch = str()

		-- list
		io.write("\nNow, write your list seperating entries with the phrase \""..ch.."\": ")
		str = str:Change(io.read())
		randlist(listeval(str(),ch))
	end
end

-- call main
main()
