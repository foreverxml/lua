#!/usr/bin/lua
-- A badly implemented "static type" for Lua.
-- DO NOT run this on its own!
-- It is meant to be used as an import.

-- Create function tables

local Class = require("class") -- class to organize, from codeberg.org/foreverxml/lua
local Static,Enum = Class:New(), Class:New()
local Meta = {
	-- if made to string, returns value
	__tostring = function(t) return t.value end,
	__call = function(t) return t.value end,
	-- index errors
	__newindex = function() error("Cannot make new index in static variable") return nil end,
	__index = function(t) return t.value end,
	-- logic + math
	__add = function(this,nex) return this[value] + nex end,
	__sub = function(this,nex) return this[value] - nex end,
	__mul = function(this,nex) return this[value] * nex end,
	__div = function(this,nex) return this[value] / nex end,
	__mod = function(this,nex) return this[value] % nex end,
	__unm = function(this,nex) return this[value] - nex end,
	__eq = function(this,nex) return this[value] == nex end,
	__lt = function(this,nex) return this[value] < nex end,
	__le = function(this,nex) return this[value] <= nex end,
	__concat = function(this,nex) return this[value]..nex end
}
-- static
function Static:New(...)
	local tab = {}
	tab.value = ...
	tab.type = type(...)
	function tab:Change(new)
		if type(new) ~= type(tab.value) then
			error("Wrong type",2)
		end
		tab.value = new
		return tab
	end
	return setmetatable(tab,Meta)
end
Static = setmetatable(Static,{__index = function(...) return Static:New(...) end})

--tysm Tjakka5/Enum github
local Metae = {
   __index    = function(_, k) error("Attempt to index non-existant enum '"..tostring(k).."'.", 2) end,
   __newindex = function()     error("Attempt to write to static enum", 2) end,
}

function Static:Enum(...)
   local values = {...}

   if type(values[1]) == "table" then
      values = values[1]
   end

   local enum = {}

   for i = 1, #values do
      enum[values[i]] = values[i]
   end

   return setmetatable(enum, Metae)
end
-- end tysm

return Static
